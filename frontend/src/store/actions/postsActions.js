import { NotificationManager } from "react-notifications";
import axiosApi from "../../axiosApi";
import { historyPush } from "./historyActions";

export const FETCH_POSTS_REQUEST = "FETCH_POSTS_REQUEST";
export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS";
export const FETCH_POSTS_FAILURE = "FETCH_POSTS_FAILURE";

export const SEND_NEW_POST_SUCCESS = 'SEND_NEW_POST_SUCCESS';
export const SEND_NEW_POST_FAILURE = 'SEND_NEW_POST_FAILURE';


const fetchPostsRequest = () => ({ type: FETCH_POSTS_REQUEST });
const fetchPostsSuccess = posts => ({ type: FETCH_POSTS_SUCCESS, posts });
const fetchPostsFailure = error => ({ type: FETCH_POSTS_FAILURE, error });


const sendNewPostSuccess = postInfo => ({ type: SEND_NEW_POST_SUCCESS, postInfo })
const sendNewPostFailure = error => ({ type: SEND_NEW_POST_FAILURE, error })


export const fetchPosts = () =>{
    return async dispatch =>{
        try {
            dispatch(fetchPostsRequest())
            const response = await axiosApi.get('/posts')
            dispatch(fetchPostsSuccess(response.data))
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(fetchPostsFailure(error.response.data))
            } else {
                dispatch(fetchPostsFailure({ global: 'No internet' }))
            }
        }
    }
}


export const sendNewPost = (postInfo) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = { 'Authorization': token };
            const response = await axiosApi.post('/posts', postInfo, { headers })
            dispatch(sendNewPostSuccess(response.data))
            dispatch(historyPush('/'))
            NotificationManager.success('Post Added!')
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(sendNewPostFailure(error.response.data))
            } else {
                dispatch(sendNewPostFailure({ global: 'No internet' }))
            }
        }


    }
}

