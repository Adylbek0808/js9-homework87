import { FETCH_POSTS_FAILURE, FETCH_POSTS_REQUEST, FETCH_POSTS_SUCCESS, SEND_NEW_POST_FAILURE, SEND_NEW_POST_SUCCESS } from "../actions/postsActions";

const initialState = {
    posts: [],
    postsLoading: false,
    postsError: null,
    recentPost: null,
    recentPostError: null
}

const postsReducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_POSTS_REQUEST:
            return { ...state, postsLoading: true }
        case FETCH_POSTS_SUCCESS:
            return { ...state, postsLoading: false, posts: action.posts }
        case FETCH_POSTS_FAILURE:
            return { ...state, postsLoading: false, postsError: action.error }
        case SEND_NEW_POST_SUCCESS:
            return { ...state, recentPost: action.postInfo }
        case SEND_NEW_POST_FAILURE:
            return { ...state, recentPostError: action.error }

        default:
            return state;
    }
}

export default postsReducer