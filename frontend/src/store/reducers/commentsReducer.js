const initialState = {
    comments: null,
    commentsLoading: false,
    commentsError: null
}

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
     
        default:
            return state;
    }
}

export default commentsReducer
