import makeStyles from '@material-ui/core/styles/makeStyles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import React, { useRef, useState } from 'react';

const useStyles = makeStyles(theme => ({
    input: {
        display: 'none'
    }
}));


const FileInput = ({ onChange, name, label }) => {
    const classes = useStyles();
    const inputRef = useRef();
    const [filename, setFilename] = useState('');

    const activateInput = () => {
        inputRef.current.click();
    };
    const onFileChange = e => {
        if (e.target.files[0]) {
            setFilename(e.target.files[0].name)
        } else {
            setFilename('')
        }
        onChange(e);
    }
    return (
        <>
            <input
                type="file"
                name={name}
                className={classes.input}
                ref={inputRef}
                onChange={onFileChange}
            />
            <Grid container spacing={2} alignItems="center">
                <Grid item xs>
                    <TextField
                        variant="outlined"
                        disabled
                        fullWidth
                        label={label}
                        onClick={activateInput}
                        value={filename}

                    />
                </Grid>
                <Grid item>
                    <Button
                        variant="contained"
                        onClick={activateInput}
                    >
                        Browse
                    </Button>
                </Grid>
            </Grid>
        </>
    );
};

export default FileInput;