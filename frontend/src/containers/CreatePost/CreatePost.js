import { Button, Grid, makeStyles, TextField, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import FileInput from '../../components/UI/Form/FileInput';
import { sendNewPost } from '../../store/actions/postsActions';

const useStyles = makeStyles(theme => ({
    formStyle: {
        margin: theme.spacing(4, 0, 12),
        border: "1px solid blue",
        borderRadius: '10px',
        padding: theme.spacing(2)
    }
}));

const CreatePost = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.posts.recentPostError)

    const [postInfo, setPostInfo] = useState({
        title: '',
        description: '',
        image: ''
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setPostInfo(prevState => ({
            ...prevState,
            [name]: value
        }));
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setPostInfo(prevState => ({
            ...prevState,
            [name]: file
        }))
    }

    const submitFormHandler = e => {
        e.preventDefault(e)
        const formData = new FormData();
        Object.keys(postInfo).forEach(key => {
            formData.append(key, postInfo[key]);
        });
        dispatch(sendNewPost(formData));

    };

    return (
        <form className={classes.formStyle} onSubmit={submitFormHandler}>
            <Grid item container direction="column" spacing={2}>
                <Grid item xs>
                    <Typography variant="h6">Create new post</Typography>
                </Grid>
                <Grid item xs>
                    <TextField
                        variant="outlined"
                        fullWidth
                        label="Title"
                        name="title"
                        onChange={inputChangeHandler}
                        required
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        variant="outlined"
                        fullWidth
                        multiline
                        rows={3}
                        label="Description"
                        name="description"
                        error={Boolean(error)}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <FileInput
                        name="image"
                        label="image"
                        error={Boolean(error)}
                        onChange={fileChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">Send</Button>
                </Grid>

            </Grid>
        </form>
    );
};

export default CreatePost;