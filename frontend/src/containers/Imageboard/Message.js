import { Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import React from 'react';
import { apiURL } from '../../config';
import chatBox from '../../assets/images/chat2.png'



const useStyles = makeStyles(theme => ({
    padding: {
        padding: theme.spacing(1)
    },
    titleSpacing: {
        marginRight: theme.spacing(2),
        color: "grey"
    },
    image: {
        height: "200px",
        display: 'inline-block',
        marginRight: theme.spacing(3)
    },
    chatPic:{
        height: "60px",
        display: 'inline-block',
        marginRight: theme.spacing(3)
    }

}))

const Message = ({ author, datetime, title, image }) => {
    const classes = useStyles();
    let pic = (
        <Grid item>
            <img src={chatBox}  className={classes.chatPic} alt="" />
        </Grid>
    );
    if (image) {
        pic = (
            <Grid item>
                <img src={apiURL + "/" + image} className={classes.image} alt="" />
            </Grid>
        );
    };

    return (
        <Grid item container direction="column" spacing={2}>
            <Paper>
                <Grid item xs className={classes.padding}>
                    <Typography variant='subtitle2' display="inline" className={classes.titleSpacing}>{author}</Typography>
                    <Typography variant='subtitle1' display="inline" className={classes.titleSpacing}>{datetime}</Typography>

                </Grid>
                <Grid item container xs className={classes.padding} >
                    {pic}
                    <Grid item xs>
                        <Typography variant='h6' display="inline"  >{title}</Typography>
                    </Grid>
                </Grid>

            </Paper>
        </Grid>
    );
};

export default Message;