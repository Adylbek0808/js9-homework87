import { CircularProgress, Grid, makeStyles } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPosts } from '../../store/actions/postsActions';
import Message from './Message';

const useStyles = makeStyles(theme => ({
    baseGrid: {
        margin: theme.spacing(0, 'auto'),
        width: '80%'
    },
    progress: {
        height: 200
    }

}))

const Imageboard = () => {
    const dispatch = useDispatch()
    const classes = useStyles();
    const posts = useSelector(state => state.posts.posts);
    const loading = useSelector(state => state.posts.postsLoading)

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={5} className={classes.baseGrid}>

            {loading ? (
                <Grid container justify="center" alignItems="center" className={classes.progress}>
                    <Grid item>
                        <CircularProgress />
                    </Grid>
                </Grid>
            ) : posts.map(post => {
                const date = new Date(post.datetime).toLocaleString()
                return (
                    <Message
                        key={post._id}
                        author={post.author.username}
                        datetime={date}
                        image={post.image}
                        title={post.title}
                    />
                )
            })}
        </Grid>
    );
};

export default Imageboard;