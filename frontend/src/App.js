import { Container, CssBaseline } from '@material-ui/core';
import { Route, Switch } from 'react-router-dom';

import AppToolbar from './components/UI/AppToolbar/AppToolbar';
import Register from './containers/Register/Register';
import Login from './containers/Login/Login'
import Imageboard from './containers/Imageboard/Imageboard';
import CreatePost from './containers/CreatePost/CreatePost';

const App = () => {
  return (
    <>
      <CssBaseline />
      <header>
        <AppToolbar />
      </header>
      <main>
        <Container maxWidth='xl'>
          <Switch>
            <Route path='/' exact component ={Imageboard}/>
            <Route path='/register' exact component={Register} />
            <Route path='/login' component={Login} />
            <Route path='/new_post' component={CreatePost}/>
          </Switch>
        </Container>
      </main>
    </>
  );
}

export default App;
