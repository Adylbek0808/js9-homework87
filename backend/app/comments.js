const express = require('express');
const userAuth = require('../middleware/userAuth');
const Comment = require('../models/Comment');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const criteria={}
        if (req.query.post_id) {
            criteria.post = req.query.post_id
        }
        const comments = await Comment.find(criteria)
        res.send(comments)
    } catch (error) {
        res.status(400).send(error)
    }
})

router.post('/', userAuth, async (req, res) => {
    try {
        const commentData = req.body                             //here will be {post: ''post_id', text:'sometext'}
        commentData.datetime = new Date().toISOString();
        commentData.author = req.user._id
        const comment = new Comment(commentData)
        await comment.save()
        res.send(comment)
    } catch (error) {
        res.status(400).send(error)
    }
})

module.exports = router;