const express = require('express');
const path = require('path')
const multer = require('multer');
const { nanoid } = require('nanoid');
const config = require('../config');

const userAuth = require('../middleware/userAuth');
const Post = require('../models/Post');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const upload = multer({ storage })

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const posts = await Post.find().sort({ datetime: -1 }).populate('author', 'username')
        res.send(posts)
    } catch (error) {
        res.status(500).send(error)
    }
});


router.post('/', [userAuth, upload.single('image')], async (req, res) => { //В req.user инфо по юзеру
    try {
        const postData = req.body
        postData.author = req.user._id
        postData.datetime = new Date().toISOString();
        if (req.file) {
            postData.image = 'uploads/' + req.file.filename
        }
        if ((postData.description.length === 0) && (!postData.image)) {
            return res.status(401).send({ error: "No description or image! Please add at least one" })
        }
        const post = new Post(postData)
        await post.save();
        return res.send(post)
    } catch (error) {
        res.status(400).send(error)
    }
})

module.exports = router