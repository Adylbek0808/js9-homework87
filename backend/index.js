const express = require('express');
const cors = require("cors");
const exitHook = require('async-exit-hook');
const mongoose = require('mongoose');
const config = require('./config')

const users = require('./app/users');
const posts = require('./app/posts');
const comments = require('./app/comments')


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/users', users);
app.use('/posts', posts);
app.use('/comments', comments)

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });

    exitHook(async callback => {
        console.log('exiting');
        await mongoose.disconnect();
        callback();
    });

}

run().catch(console.error)