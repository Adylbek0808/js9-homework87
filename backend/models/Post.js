const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        required: true,
    },
    description: String,
    datetime: {
        type: String,
        required: true
    },
    image: String
})

// PostSchema.set('toJSON', {
//     transform: (doc, ret, options) => {
//         delete ret.author;
//         return ret;
//     }
// });

const Post = mongoose.model('Post', PostSchema);
module.exports = Post