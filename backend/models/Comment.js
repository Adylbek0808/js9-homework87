const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    post: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment',
        required: true
    },
    text: {
        type: String,
        required: true
    },
    datetime: {
        type: String,
        required: true
    }
})

// CommentSchema.set('toJSON', {
//     transform: (doc, ret, options) => {
//         delete ret.author;
//         return ret;
//     }
// });

const Comment = mongoose.model('Comment', CommentSchema);
module.exports = Comment;